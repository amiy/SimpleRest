const Router = require('koa-router');

const Model = require('../db/model');

const router = new Router();

const BASE_URL = `/api/v1/location`;

router.get('/', async ctx => {
	try {
		const infos = await Model.find({});
		console.log('res is :' + infos);
		ctx.body = {
			success: true,
			data: infos,
		};
	} catch (e) {
		console.log(e);
	}
});

router.post('/', async ctx => {
	console.log('sss');
	const req = ctx.body;

	console.log(ctx.params);
	console.log(ctx.req.body);
	try {
		const test = new Model({
			location: { longitude: 32.3332, latitude: 21.332 },
			data: [1, 2, 3],
		});
		test.save();
	} catch (e) {}
});

module.exports = router;

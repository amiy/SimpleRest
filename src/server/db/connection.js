const mongoose = require('mongoose');

const { MONGODB_URL } = require('../config');

async function connect() {
	mongoose.Promise = Promise;
	await mongoose.connect(MONGODB_URL);
	return mongoose.connection;
}

let conn = null;

async function initDB() {
	console.log(MONGODB_URL);
	conn = await connect();
	return conn;
}

async function resetDb() {
	await conn.dropDatabase();
}

module.exports = { initDB };

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const schema = new Schema({
	location: {
		latitude: Number,
		longitude: Number,
	},
	data: [Number],
	key: String,
});

const Model = mongoose.model('info', schema);

module.exports = Model;

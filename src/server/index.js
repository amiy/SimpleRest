const Koa = require('koa');
const bodyParser = require('koa-bodyparser');

const locationRoute = require('./routes/location');
const { initDB } = require('./db/connection');

const app = new Koa();
const PORT = process.env.PORT || 1337;
// sessions
app.keys = ['super-secret-key'];

// body parser
app.use(bodyParser());

// routes
app.use(locationRoute.routes());

// server
const server = app.listen(PORT, async () => {
	await initDB();
	console.log(`Server listening on port: ${PORT}`);
});

module.exports = server;

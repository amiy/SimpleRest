const path = require('path');

const BASE_PATH = path.join(__dirname, 'src', 'server', 'db');

module.exports = {
	test: {
		client: 'pg',
		connection: {
			database: 'postgres://localhost:5432/locale',
			user: 'agent',
			password: 'agent1234',
		},
		migrations: {
			directory: path.join(BASE_PATH, 'migrations'),
		},
		seeds: {
			directory: path.join(BASE_PATH, 'seeds'),
		},
	},

	development: {
		client: 'pg',
		connection: 'postgres://agent:agent1234@localhost:5432/locale',
		migrations: {
			directory: path.join(BASE_PATH, 'migrations'),
		},
		seeds: {
			directory: path.join(BASE_PATH, 'seeds'),
		},
	},
};
